var gulp = require('gulp')
var browserSync = require('browser-sync').create()
var cleanCSS = require('gulp-clean-css')
var htmlmin = require('gulp-htmlmin')
var concat = require('gulp-concat')
var uglify = require('gulp-uglify')
var imagemin = require('gulp-imagemin')
var sourcemaps = require('gulp-sourcemaps')
var del = require('del')
var copy = require('gulp-copy')

//
// ASSET -> source
// BUILD -> target
//

var paths = {
  scripts: [
    'assets/js/vendor/modernizr-2.8.3.min.js', 
    'assets/js/vendor/jquery-2.1.4.min.js',
    'assets/js/vendor/google-fonts.js',
    'assets/js/vendor/jquery.easing.js',
    'assets/js/vendor/jquery.waypoints.min.js',
    'assets/js/vendor/bootstrap.min.js',
    'assets/js/vendor/bootstrap-hover-dropdown.min.js',
    'assets/js/vendor/smoothscroll.js',
    'assets/js/vendor/jquery.localScroll.min.js',
    'assets/js/vendor/jquery.scrollTo.min.js',
    'assets/js/vendor/jquery.stellar.min.js',
    'assets/js/vendor/jquery.parallax.js',
    'assets/js/vendor/jquery.easypiechart.min.js',
    'assets/js/vendor/countup.min.js',
    'assets/js/vendor/isotope.min.js',
    'assets/js/vendor/jquery.magnific-popup.min.js',
    'assets/js/vendor/wow.min.js',
    'assets/js/vendor/jquery.ajaxchimp.js',
    'assets/js/vendor/slick.min.js',
    'assets/js/gmap.js',
    'assets/js/main.js'
  ],
  images: ['assets/images/*.jpg', 'assets/images/*.png', 'assets/images/*.gif', 'assets/images/*.svg'],
  css: ['assets/css/**/*.css', 'assets/css/*.css'],
  output: 'build',
  fonts: 'assets/fonts/**/*.*',
  html: 'assets/html/index.html',
  favico: 'assets/ico/favicon.ico'
}

gulp.task('clean', function() {
  return del(['build'])
})

gulp.task('fonts', ['clean'], function() {
  return gulp.src(paths.fonts)
    .pipe(copy(paths.output, {"prefix": 1}))
})

gulp.task('html', ['clean'], function() {
  return gulp.src(paths.html)
    .pipe(htmlmin({collapseWhitespace: true, removeComments: true}))
    .pipe(gulp.dest(paths.output))
})

gulp.task('favico', ['clean'], function() {
  return gulp.src(paths.favico)
    .pipe(copy(paths.output,{"prefix": 2}))
})

gulp.task('minify-css', ['clean'], function() {
  return gulp.src(paths.css)
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(concat('styles.min.css'))
    .pipe(gulp.dest(paths.output + '/css'))
})

gulp.task('scripts', ['clean'], function() {
  return gulp.src(paths.scripts)
    .pipe(sourcemaps.init())
      .pipe(uglify())
      .pipe(concat('all.min.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('build/js'))
})

gulp.task('images', ['clean'], function() {
  return gulp.src(paths.images)
    .pipe(imagemin({optimizationLevel: 9}))
    .pipe(gulp.dest('build/images'))
})

gulp.task('build', ['fonts', 'html', 'favico', 'minify-css', 'scripts', 'images'])

// create a task that ensures the `build` task is complete before
// reloading browsers
gulp.task('build-watch', ['build'], function(done) {
    browserSync.reload()
    done()
})

// use serve task to launch BrowserSync and watch HTML files
gulp.task('serve', ['build'], function() {
    // Serve files from the root of this project
    browserSync.init({
        server: {
            baseDir: "./build"
        }
    })
    // add browserSync.reload to the tasks array to make
    // all browsers reload after tasks are complete.
    gulp.watch(paths.html, ['build-watch']);
    gulp.watch(paths.images, ['build-watch']);
    gulp.watch(paths.css, ['build-watch']);
})

gulp.task('default', ['serve'])
