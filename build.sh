#!/usr/bin/env bash

# install dependencies
yarn install

# build site
yarn run build

#replace your profile here
# 2419200 seconds - 28 days, cache time
aws --profile istvan s3 sync . s3://www.streambright.io/ \
  --cache-control 'max-age=2419200' \
  --acl public-read \
  --output json \
  --region us-east-1
