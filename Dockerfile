FROM node:8.2.1

ENV PORT 3000
EXPOSE 3000

COPY package.json package.json
RUN npm install

# Add source files
COPY . .

CMD ["npm", "start"]