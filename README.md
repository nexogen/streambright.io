StreamBright Inc. a NEXOGEN Company
# streambright.io

1. Before doing anything:
    yarn install

2. Use BrowserSync for development:
    yarn start

3. Build website for deployment:
    yarn build

4. Deploy to Amazon S3:
    ./build.sh

# Docker

1. docker build -t nexogen/streambright.io:latest .
2. docker run -d -p 3000:3000 nexogen/streambright.io:latest